using System.Text.Json.Serialization;

namespace CA.Core.Models;

public record Customer(
    [property: JsonPropertyName("id")]
    Guid Id, 
    [property: JsonPropertyName("fullname")]
    string Fullname
);


// Example 2
// public record QuoteResponse
// {
//     [JsonPropertyName("quotes")]
//     public IReadOnlyCollection<Quote>? Quotes {get; init;}
// }