using CA.Presentation.Configurations;
using CA.Presentation.EndpointDefinitions;

var builder = WebApplication.CreateBuilder(args);

// Configure Services
builder.Services.RegisterServices();

var app = builder.Build();

// Configure 
app.UseSwagger();
app.UseSwaggerUI();

// app.UseAuthentication(); // After because swagger is going to be public.
// app.UseAuthorization();

CustomerEndpointDefinition.CreateCustomerEndpointDefinitions(app);

// Run
app.Run();

public partial class Program
{
    // Expose the Program class for use with WebApplicationFactory<T>
}
