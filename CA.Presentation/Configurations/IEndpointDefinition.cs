namespace CA.Presentation.Configurations;

public interface IEndpointDefinition
{
    void DefineEndpoints(WebApplication app);

    void DefineServices(IServiceCollection services);
}
