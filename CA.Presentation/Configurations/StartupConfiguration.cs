using CA.Application.Interfaces;
using CA.Application.Services;
using CA.Infrastructure;

namespace CA.Presentation.Configurations;

public static class StartupConfiguration
{
    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddScoped<ICustomerRepository, CustomerRepository>();
        services.AddScoped<ICustomerService, CustomerService>();

        // services.AddAuthentication();
        // services.AddAuthorization();
        
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
    }
}
