using CA.Application.Interfaces;
using CA.Core.Models;

namespace CA.Presentation.EndpointDefinitions;
public static class CustomerEndpointDefinition 
{
    public static void CreateCustomerEndpointDefinitions(WebApplication app)
    {   
        app.MapGet("/customers", CustomerEndpointDefinition.GetAllCustomers).Produces<List<Customer>>(204);
        app.MapGet("/customers/{id}", CustomerEndpointDefinition.GetCustomerById).Produces<Customer>(200);
        app.MapPost("/customers", CustomerEndpointDefinition.CreateCustomer).Produces<Customer>(201);
        app.MapPut("/customers/{id}", CustomerEndpointDefinition.UpdateCustomer).Produces<Customer>(200);
        app.MapDelete("/customers/{id}", CustomerEndpointDefinition.DeleteCustomer).Produces(204);
    }

    internal static IResult GetAllCustomers(ICustomerService service)
    {
        return Results.Ok(service.GetAll());
    }

    internal static IResult GetCustomerById(ICustomerService service, Guid id)
    {
        var customer = service.GetById(id);
        return customer is not null ? Results.Ok(customer) : Results.NotFound();
    }
    internal static IResult CreateCustomer(ICustomerService service, Customer customer)
    {
        service.Create(customer);
        return Results.Created($"/customers/{customer.Id}", customer);
    }

    internal static IResult UpdateCustomer(ICustomerService service, Guid id, Customer updateCustomer)
    {
        var customer = service.GetById(id);

        if (customer is null)
            return Results.NotFound();

        service.Update(updateCustomer);
        return Results.Ok(updateCustomer);
    }

    internal static IResult DeleteCustomer(ICustomerService service, Guid id)
    {
        var customer = service.GetById(id);
            
        if (customer is null)
            return Results.NotFound();

        service.Delete(id);
        return Results.Ok();
    }
}
