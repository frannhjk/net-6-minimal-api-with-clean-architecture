using Xunit;
using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using CA.Core.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CA.Application.Interfaces;
using NSubstitute;
using Shouldly;
using System.Net.Http;

namespace CA.Testing;

public class CustomerEndpointsTests
{
    private readonly ICustomerService _customerService = 
        Substitute.For<ICustomerService>();

    // private readonly WebApplicationFactory<Program> _webApplicationFactory;

    // public CustomerEndpointsTests()
    // {
    //     _webApplicationFactory = new WebApplicationFactory<Program>();
    // }

    // public HttpClient SystemUnderTest =>
    //     _webApplicationFactory.CreateClient();
    
    [Fact]
    public async Task GetCustomerById_ReturnCustomer_WhenCustomerExists()
    {
        // Arrange
        var id = Guid.NewGuid();
        var customer = new Customer(id, "Franco Dugo");
        _customerService.GetById(Arg.Is(id)).Returns(customer);

        using var app = new CustomerEndpointsTestsApp(x => 
        {
            x.AddSingleton(_customerService);
        });

        var httpClient = app.CreateClient();

        // Act
        var response = await httpClient.GetAsync($"/customers/{id}");
        var responseText = await response.Content.ReadAsStringAsync();
        var customerResult = JsonSerializer.Deserialize<Customer>(responseText);

        // Assert
        response.StatusCode.ShouldBe(HttpStatusCode.OK);
        customerResult.ShouldBeEquivalentTo(customer);
    }

    [Fact]
    public async Task GetCustomerById_ReturnCustomer_WhenCustomerDoesntExists()
    {
        // Arrange
        var id = Guid.NewGuid();

        using var app = new CustomerEndpointsTestsApp(x => 
        {
            x.AddSingleton(_customerService);
        });

        var httpClient = app.CreateClient();

        // Act
        var response = await httpClient.GetAsync($"/customers/{id}");
        var responseContent = await response.Content.ReadAsStringAsync();
       
        // Assert
        response.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        responseContent.ShouldBeEmpty();
    }
}

internal class CustomerEndpointsTestsApp : WebApplicationFactory<Program>
{
    private readonly Action<IServiceCollection> _serviceOverride;

    public CustomerEndpointsTestsApp(Action<IServiceCollection> serviceOverride)  
    {
        _serviceOverride = serviceOverride;
    }

    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureServices(_serviceOverride);
        return base.CreateHost(builder);
    }
}
