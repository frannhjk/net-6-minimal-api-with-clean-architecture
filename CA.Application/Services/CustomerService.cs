using CA.Application.Interfaces;
using CA.Core.Models;

namespace CA.Application.Services;

public class CustomerService : ICustomerService
{
    private readonly ICustomerRepository _repository;
    
    public CustomerService(ICustomerRepository repository)
    {
        _repository = repository;
    }

    public void Create(Customer? customer)
    {
        _repository.Create(customer);
    }

    public void Delete(Guid id)
    {
        _repository.Delete(id);
    }

    public List<Customer> GetAll()
    {
        return _repository.GetAll();
    }

    public Customer? GetById(Guid id)
    {
        return _repository.GetById(id);
    }

    public void Update(Customer customer)
    {
       _repository.Update(customer);
    }
    
}
